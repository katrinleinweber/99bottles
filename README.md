# 99 Bottles of OOP (annotated)

A fork of [GitHub.com/sandimetz/99bottles_ruby](https://github.com/sandimetz/99bottles_ruby)
with summaries / learnings in the commit messages.

That way, [the `git blame` view of `lib/bottles.rb`](https://gitlab.com/katrinleinweber/99bottles/-/blame/main/lib/bottles.rb),
and the [commits list](https://gitlab.com/katrinleinweber/99bottles/-/commits/main)
should be useful for reviewing, recalling and learning.

Context: [gitlab-com/book-clubs#4](https://gitlab.com/gitlab-com/book-clubs/-/issues/24)

[Read a sample of the book](https://sandimetz.com/99bottles-sample-ruby)
and [buy it](https://sandimetz.com/99bottles). It is very good.
